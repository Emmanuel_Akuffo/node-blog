const express = require('express')
const mongoose = require('mongoose')
const Article = require('./models/article')
const articleRouter = require('./routes/articles')
const methodOverride = require('method-override')
const app = express()

//DB connection
mongoose.connect("mongodb+srv://emmanuel:whiteeagle@cluster0-6x9ra.mongodb.net/Blog?retryWrites=true&w=majority",
  {
     useNewUrlParser: true ,
     useUnifiedTopology: true,
     useCreateIndex : true
  
  }
  )


mongoose.connection.on('connected', ()=>{
  console.log("Database connected.....")
})

app.set('view engine', 'ejs')
app.use(express.urlencoded({ extended: false }))
app.use(methodOverride('_method') )

app.get('/', async (req, res) => {
  const articles = await Article.find().sort({ createdAt: 'desc' })
  res.render('articles/index', { articles: articles })
})

app.use('/articles', articleRouter)

app.listen(5000)